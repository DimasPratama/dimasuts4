/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import splashScreen from './splashScreen';
import loginScreen from './prasmulPay/forLogin/loginScreen';
import orderMainPage from './prasmulPay/orderMain/orderMainPage';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => orderMainPage);
