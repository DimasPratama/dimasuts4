/**
 * @format
 */

// import {AppRegistry} from 'react-native';
import App from './App';
import App2 from './App2';
import SideMenu from './SideMenu'
import WebPay from './WebPay'
import Menu from './prasmulPay/forMenu/Menu'
import orderMainPage from './prasmulPay/orderMain/orderMainPage'
import loginScreen from './prasmulPay/forLogin/loginScreen'
// import {name as appName} from './app.json';
import {Navigation} from "react-native-navigation";


// AppRegistry.registerComponent(appName, () => App);

Navigation.registerComponent('App1', () => App);
Navigation.registerComponent('App2', () => App2);
Navigation.registerComponent('SideMenu', () => SideMenu);
Navigation.registerComponent('WebPay', () => WebPay);
Navigation.registerComponent('orderMainPage', () => orderMainPage);
Navigation.registerComponent('Menu', () => Menu);
Navigation.registerComponent('loginScreen', () => loginScreen);

Navigation.events().registerAppLaunchedListener(() => {
    Navigation.setDefaultOptions({
        topBar:{
            visible: false,
            animate : false,
            drawBehind:false,


        }

    });





    Navigation.setRoot({
        root: {
            stack: {
                children: [{
                    component: {
                        name: 'loginScreen',
                    }
                }],
                options: {}
            }



        }
    });
});