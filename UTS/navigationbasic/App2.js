/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flw
 */

import React, {Component} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View, TextInput} from 'react-native';
import {Navigation} from "react-native-navigation";


export default class App2 extends Component {

    onPress = () => {
        console.log('push to App 2')
        // Navigation.push(this.props.componentId, {
        //     component: {
        //         name: 'App1',
        //
        //     }
        // });

        Navigation.showOverlay({
            component: {
                name: 'App1',
                // options: {
                //     overlay: {
                //         interceptTouchOutside: true
                //     }
                // }
            }
        });

    }


  render() {
    return (
      <View>
          <View style={styles.HeaderList}>
              <Text style={{fontSize:22, color:'black'}}>
                  List Menu
              </Text>
          </View>


          <View>
            <View style={styles.containerMoney}>
                <View style={{marginLeft:20,marginTop: 10}}>
                    <Text style={{fontSize:18,fontWeight: '800',color:'#FF9345'}}>Rp. 875.000,-</Text>
                    <Text style={{fontSize:12,color:'#979595'}}>Cash in PrasmulPay</Text>

                </View>

                <TouchableOpacity style={styles.Button}>
                    <Text style={{fontSize:12,color:'white'}}>
                        Check Mutation
                    </Text>
                </TouchableOpacity>
            </View>

              <View style={{flexDirection: 'row',marginTop:20,marginLeft:20}}>

              {/*fotokios dan edit*/}
              <View style={{flex:1,alignItems:'center'}}>
                  <Image
                  source={require('./img/icon/tokoProfile.png')}
                  />
                  <TouchableOpacity>
                      <Text style={{color:'#008FAE',marginTop:5}}>Edit</Text>
                  </TouchableOpacity>

              </View>
                {/*untuk nama kios dan deskripsi*/}
              <View style={{flex:3,marginHorizontal:30}}>
                  <View style={{flexDirection:'row',justifyContent:'space-between',marginLeft:20}}>
                      <View style={{marginTop:5}}>
                      <TextInput style={{color:'#008FAE',fontSize:16}}>
                          <Text style={{color:'#008FAE',fontSize:16}}>Kantin KITA</Text>
                      </TextInput>

                      </View>

                      <Image source={require('./img/icon/Edit_icon.png')}/>

                  </View>
                  <TextInput style={styles.boxInput}
                      placeholderTextColor="#494949"
                      placeholder="Text in Description"
                        multiline={true}>
                  </TextInput>
              </View>

              </View>

              <View style={{marginBottom:2}}>

                  <View style={styles.subContainer}>
                      <Text style={styles.subTitleProfile}>USERNAME</Text>
                  </View>

                  <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                      <View>
                      <TextInput style={{color:'#008FAE',fontSize:16,marginLeft:20,marginTop:10}}>
                          <Text style={{color:'#008FAE',fontSize:16,fontWeight:'500'}}>YaaKita</Text>
                      </TextInput>
                      </View>

                      <Image
                          style={{marginTop:7,marginRight: 25}}
                          source={require('./img/icon/Edit_icon.png')}
                      />


                  </View>
              </View>

              <View style={{marginBottom:2}}>

                  <View style={styles.subContainer}>
                      <Text style={styles.subTitleProfile}>MERCHANT EMAIL</Text>
                  </View>

                  <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                      <View>
                          <TextInput style={{color:'#008FAE',fontSize:16,marginLeft:20,marginTop:10}}>
                              <Text style={{color:'#008FAE',fontSize:16,fontWeight:'500'}}>kitabisaiya@gmail.com</Text>
                          </TextInput>
                      </View>

                      <Image
                          style={{marginTop:7,marginRight: 25}}
                          source={require('./img/icon/Edit_icon.png')}
                      />


                  </View>
              </View>


              <View style={{marginBottom:2}}>

                  <View style={styles.subContainer}>
                      <Text style={styles.subTitleProfile}>PHONE NUMBER</Text>
                  </View>

                  <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                      <View>
                          <TextInput style={{color:'#008FAE',fontSize:16,marginLeft:20,marginTop:10}}>
                              <Text style={{color:'#008FAE',fontSize:16,fontWeight:'500'}}>081219135900</Text>
                          </TextInput>
                      </View>

                      <Image
                          style={{marginTop:7,marginRight: 25}}
                          source={require('./img/icon/Edit_icon.png')}
                      />


                  </View>
              </View>
              <View style={{marginBottom:2}}>

                  <View style={styles.subContainer}>
                      <Text style={styles.subTitleProfile}>PASSWORD</Text>
                  </View>

                  <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                      <View>
                          <TextInput style={{color:'#008FAE',fontSize:16,marginLeft:20,marginTop:10}}
                                     secureTextEntry>
                              <Text style={{color:'#008FAE',fontSize:16,fontWeight:'500'}}>YaaKita</Text>
                          </TextInput>
                      </View>

                      <Image
                          style={{marginTop:7,marginRight: 25}}
                          source={require('./img/icon/Edit_icon.png')}
                      />


                  </View>
              </View>



          </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerMoney: {
      justifyContent: 'space-between',
      flexDirection: 'row',

  },
    HeaderList:{
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom:20,
        marginTop: 40,
        borderBottomWidth:12,
        borderBottomColor: '#E5E5E5',
        width:400


    },
    Button:{
        backgroundColor:'#008FAE' ,
        borderRadius:5,
        height:21,
        width: 100,
        alignItems:'center',
        justifyContent: 'center',
        marginTop:15,
        marginRight:20
    },
  boxInput: {
    marginLeft:20,
      marginTop:5,
      borderRadius:10,
    backgroundColor:'#E7E7E7',
    height: 100,
      width:200,
      fontSize:11,
      paddingLeft:10
  },
    subTitleProfile: {
      fontSize:11,
      color:'#737373',
        paddingTop:5
    },

    subContainer:{
      marginTop:10,
      paddingLeft:20,
      height: 25,
      width: '100%',
      backgroundColor:'#F0F0F0'
  }
});
