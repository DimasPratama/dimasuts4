/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import { StyleSheet, Text, TextInput, View, TouchableOpacity, Image, KeyboardAvoidingView} from 'react-native';
import {Navigation} from "react-native-navigation";

export default class loginScreen extends Component{

    _loginButtonPressed = () => {
    Navigation.setRoot({
                           root: {
                               bottomTabs: {
                                   children: [
                                       {
                                           stack: {
                                               children: [{
                                                   component: {
                                                       name: 'orderMainPage'
                                                   },
                                               }],
                                               options: {
                                                   bottomTab: {
                                                       text: 'Home',
                                                       icon: require('../../img/icon/house-outline.png')
                                       }
                                       }

                                       }
                                       },
                                       {
                                           stack: {
                                               children: [{
                                                   component: {
                                                       name: 'Menu'
                                                   },
                                               }],
                                               options: {
                                                   bottomTab: {
                                                       text: 'Menu',
                                                       icon: require('../../img/icon/Component.png')
                                       }
                                       }
                                       }
                                       },
                                       {
                                           stack: {
                                               children: [{
                                                   component: {
                                                       name: 'App2'
                                                   },
                                               }],
                                               options: {
                                                   bottomTab: {
                                                       text: 'Profile',
                                                       icon: require('../../img/icon/user.png')
                                       }
                                       }
                                       }
                                       },

                                   ],
                                   options: {}
                               }
                           }
                       });
    };

    // onPress = () => {
    //     console.log('push to orderMainPage')
    //     Navigation.push(this.props.componentId, {
    //         component: {
    //             name: 'orderMainPage',
    //
    //         }
    //     });
    //
    // }

        render() {
        return (
            <View style={styles.container}>

                <View style={styles.logoContainer}>

                    <Text style={styles.logo}>
                        PrasmulPay
                    </Text>

                    <Text style={styles.instructions}>
                        Merchant
                    </Text>

                </View>
                <KeyboardAvoidingView style={styles.containerInput}>

                    {/*for email*/}
                    <View style={styles.inputContainer}>

                        <Image source={require('../../img/icon/username.png')} />
                        <View style={styles.EmailPass}>
                        <TextInput
                        style={{fontSize:16,color:'#008FAE',width:200}}
                        placeholderTextColor="#008FAE"
                        placeholder="Username"
                        returnKeyType="next"
                        onSubmitEditing={()=>this.passwordInput.focus()}
                        />
                        </View>
                    </View>

                    {/*for password*/}
                    <View style={styles.inputContainer}>

                        <Image source={require('../../img/icon/password.png')} />
                        <View style={styles.EmailPass}>
                        <TextInput
                        style={{fontSize:16,color:'#008FAE',width:200}}
                        placeholderTextColor="#008FAE"
                        secureTextEntry
                        placeholder="Password"
                        ref={(input)=>this.passwordInput = input}
                        />
                       </View>
                    </View>

                    <TouchableOpacity
                        onPress={this._loginButtonPressed}
                        style={styles.Button}>
                        <Text style={styles.ButtonText}>LOG IN</Text>
                    </TouchableOpacity>

                </KeyboardAvoidingView>

                <View style={styles.containerForget}>

                    <TouchableOpacity >
                        <Text style={{color:'#BCBCBC'}}>Forget Password ?</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.container}>
                   <Text style={{marginBottom:10}}>
                       Don't have any account ?
                   </Text>
                    <TouchableOpacity >
                        <Text style={{color:'#008FAE'}}>Create Account</Text>
                    </TouchableOpacity>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    logoContainer: {
        flex:1,
        paddingTop:100,
        margin: 10,
    },
    logo: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#FF9345'

    },
    instructions: {
        fontSize:18,
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
        fontWeight: 'bold',
        fontStyle: 'italic',
        textDecorationLine: 'underline'
    },

    containerInput:{
        flex:1,
        alignItems:'center',
        paddingBottom: 40,


    },
    inputContainer:{
        flexDirection:'row',
        width:270,
        marginBottom: 20,
        borderBottomWidth:1,
        borderBottomColor: 'black',
        paddingVertical:5
    },
    EmailPass:{
        paddingLeft:20,
        color:'#008FAE' ,

    },
    Button:{
        backgroundColor:'#008FAE' ,
        borderRadius:20,
        height:40,
        width: 300,
        alignItems:'center',
        justifyContent: 'center',
        marginTop:20,
    },
    ButtonText: {
        color:'white',
        fontSize:18,

    },
    containerForget: {
        flex: 1,
        alignItems: 'center',
        justifyContent:'center',
        backgroundColor: '#F5FCFF',
    },
});
