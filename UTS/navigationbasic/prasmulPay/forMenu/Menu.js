import React, { Component } from 'react';

import {
    Alert,
    LayoutAnimation,
    StyleSheet,
    View,
    Text,
    ScrollView,
    UIManager,
    TouchableOpacity,
    Platform,
    Image,
    TextInput
} from 'react-native';
import {Navigation} from "react-native-navigation";

class Expandable_ListView extends Component {

    constructor() {

        super();

        this.state = {

            layout_Height: 0

        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.item.expanded) {
            this.setState(() => {
                return {
                    layout_Height: null
                }
            });
        }
        else {
            this.setState(() => {
                return {
                    layout_Height: 0
                }
            });
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.state.layout_Height !== nextState.layout_Height) {
            return true;
        }
        return false;
    }

    show_Selected_Category = (item) => {

        // Write your code here which you want to execute on sub category selection.
        Alert.alert(item);

    }

    render() {
        return (
            <View style={styles.Panel_Holder}>

                <TouchableOpacity activeOpacity={0.8} onPress={this.props.onClickFunction} style={styles.category_View}>
                    <View style={{flexDirection:'row'}}>
                    <Image
                        source={require('../../img/icon/Makanan.png')}
                         />

                    <Text style={styles.category_Text}>{this.props.item.category_Name} </Text>
                    </View>
                    <Image
                        source={{ uri: 'https://reactnativecode.com/wp-content/uploads/2019/02/arrow_right_icon.png' }}
                        style={styles.iconStyle} />

                </TouchableOpacity>

                <View style={{ height: this.state.layout_Height, overflow: 'hidden' }}>

                    {
                        this.props.item.sub_Category.map((item, key) => (

                            <View  style={{backgroundColor:'#FF9345',borderBottomLeftRadius:10,borderBottomRightRadius:10,paddingTop: -20}}>
                                <View  style={{flexDirection:'row', height: 25, marginBottom: 10}}>
                                    <Text style={styles.textDesc}> Stock Available </Text>

                                    {/*stepper untuk nambah dan ngurang*/}
                                    <View style={{flexDirection: 'row',}}>
                                        <TouchableOpacity>
                                            <View style={[styles.stepperButton, {borderBottomLeftRadius: 8, borderTopLeftRadius: 8}]}>
                                                <Text style={{fontWeight: "800", fontSize: 15, color: '#fff'}}>-</Text>
                                            </View>
                                        </TouchableOpacity>

                                        <View style={styles.stepperButton}>
                                        <Text style={{fontWeight: "800", fontSize: 15, color: '#fff'}}>1</Text>
                                        </View>

                                        <TouchableOpacity>

                                            <View style={[styles.stepperButton, {borderBottomRightRadius: 8, borderTopRightRadius: 8}]}>
                                                <Text style={{fontWeight: "800", fontSize: 15, color: '#fff'}}>+</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>

                                </View>

                                <View style={{flexDirection:'row'}}>
                                    <Text style={styles.textDesc}> Description :</Text>
                                    <TextInput
                                        style={styles.textInput}
                                        placeholderTextColor='white'
                                        placeholder="This is description"
                                        multiline={true}/>
                                </View>
                            </View>

                            // <TouchableOpacity key={key} style={styles.sub_Category_Text} onPress={this.show_Selected_Category.bind(this, item.name)}>
                            //
                            //
                            // </TouchableOpacity>

                        ))
                    }

                </View>

            </View>

        );
    }
}

export default class Menu extends Component {

    onPress = () => {
        console.log('push to App')
        Navigation.push(this.props.componentId, {
            component: {
                name: 'App1',

            }
        });
    }


    constructor() {
        super();



        if (Platform.OS === 'android') {

            UIManager.setLayoutAnimationEnabledExperimental(true)

        }

        const array = [

            {
                expanded: false, category_Name: "Mie Ayam", sub_Category: [{ id: 1, name: 'Mi' }]
            },

            {
                expanded: false, category_Name: "Nasi Goreng", sub_Category: [{ id: 8, name: 'Dell' }]
            },

            {
                expanded: false, category_Name: "Mie Goreng", sub_Category: [{ id: 12, name: 'Pendrive' }]
            },

            {
                expanded: false, category_Name: "Kwetiau", sub_Category: [{ id: 16, name: 'Home Audio Speakers' }]
            }
        ];

        this.state = { AccordionData: [...array] }
    }

    update_Layout = (index) => {

        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);

        const array = [...this.state.AccordionData];

        array[index]['expanded'] = !array[index]['expanded'];

        this.setState(() => {
            return {
                AccordionData: array
            }
        });
    }

    render() {
        return (
            <View style={styles.MainContainer}>

                    <View style={styles.HeaderList}>
                        <Text style={{fontSize:22, color:'black'}}>
                            List Menu
                        </Text>
                    </View>


                <ScrollView contentContainerStyle={{ paddingHorizontal: 10, paddingVertical: 5 }}>
                    {
                        this.state.AccordionData.map((item, key) =>
                            (
                                <Expandable_ListView key={item.category_Name} onClickFunction={this.update_Layout.bind(this, key)} item={item} />
                            ))
                    }
                </ScrollView>


                <View style={{alignItems:'center'}}>
                    <TouchableOpacity style={styles.buttonAll} onPress={this.onPress}>
                        <Text style={{color:'white',fontSize:18}}>Add Menu</Text>
                    </TouchableOpacity>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    HeaderList:{
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom:20,
        marginTop: 30,
        borderBottomWidth:12,
        borderBottomColor: '#E5E5E5',
        width:400


    },

    MainContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems:'center',
        paddingTop: (Platform.OS === 'ios') ? 20 : 0,
        backgroundColor: '#F5FCFF',
    },

    iconStyle: {

        width: 30,
        height: 30,
        justifyContent: 'flex-end',
        alignItems: 'center',
        tintColor: '#fff'

    },

    sub_Category_Text: {
        fontSize: 18,
        color: '#000',
        padding: 10
    },

    category_Text: {
        textAlign: 'left',
        color: '#fff',
        fontSize: 21,
        padding: 10
    },

    category_View: {
        marginTop: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#FF9345',
        paddingBottom: 10,
        width:300,
        borderTopRightRadius:10,
        borderTopLeftRadius: 10

    },

    Btn: {
        padding: 10,
        backgroundColor: '#FF6F00'
    },

    textInput:{
        marginLeft:10,
        marginBottom:10,
        width:200,
        height:80,
        backgroundColor: '#FFAF76',
        color: 'white',
        borderRadius:5

    },
    textDesc:{
        paddingBottom:10,
        fontSize:12,
        color:'white'

    },
    stepperButton: {
        backgroundColor: '#FFAF76',
        alignItems: 'center',
        justifyContent: 'center',
        width: 25,
        height: '100%'
    },
    buttonAll: {
    marginVertical:10,
    backgroundColor:'#008FAE',
    alignItems:'center',
    justifyContent:'center',
    width:320,
    height:40,
    borderRadius: 20
    }



});