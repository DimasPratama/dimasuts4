

import React, {Component} from 'react';
import {Image, StyleSheet, Text, View,TouchableOpacity,ScrollView} from 'react-native';



export default class orderMainPage extends Component {


    render() {
        return (

            <View style={styles.container}>

                {/*untuk Header*/}
                <View style={styles.whiteBox}>
                    <Image source={require('../../img/icon/kios.png')} />
                    <View style={styles.whiteBoxtext}>

                    <Text>Kantin Kita</Text>
                    <Text>Status:
                    <Text style={{color:'green'}}>OPEN</Text>
                    </Text>

                    </View>
                    <TouchableOpacity>
                    <Image
                        style={{marginLeft:150}}
                        source={require('../../img/icon/NotificationBell.png')} />
                    </TouchableOpacity>
                </View>

                {/*untuk item*/}


            <ScrollView>
                <View style={styles.blueBox}>
                    <View style={styles.blueBoxPicText}>

                        <View style={styles.blueBoxText}>
                            <View style={styles.boxTitle}>
                            <Text style={styles.blueBoxTitle}>Adit Sanajaya</Text>
                            </View>
                            <Text style={styles.blueBoxDesc}>   - Ayam Goreng</Text>
                            <Text style={styles.blueBoxDesc}>   - Sup Ayam</Text>
                            <Text/>
                            <Text style={styles.blueBoxDesc}> Take : 21 januari 2018</Text>
                            <Text style={styles.blueBoxDesc}> Note :tidak pedas</Text>
                        </View>

                        <Image
                            style={styles.blueboxPic}
                            source={require('../../img/icon/akunBabi.png')} />
                    </View>
                </View>

                <View style={styles.blueBox}>
                    <View style={styles.blueBoxPicText}>

                        <View style={styles.blueBoxText}>
                            <View style={styles.boxTitle}>
                                <Text style={styles.blueBoxTitle}>Adit Sanajaya</Text>
                            </View>
                            <Text style={styles.blueBoxDesc}>   - Ayam Goreng</Text>
                            <Text style={styles.blueBoxDesc}>   - Sup Ayam</Text>
                            <Text/>
                            <Text style={styles.blueBoxDesc}> Take : 21 januari 2018</Text>
                            <Text style={styles.blueBoxDesc}> Note :tidak pedas</Text>
                        </View>
                        <Image
                            style={styles.blueboxPic}
                            source={require('../../img/icon/akunBabi.png')} />
                    </View>
                </View>

                <View style={styles.blueBox}>
                    <View style={styles.blueBoxPicText}>

                        <View style={styles.blueBoxText}>
                            <View style={styles.boxTitle}>
                                <Text style={styles.blueBoxTitle}>Adit Sanajaya</Text>
                            </View>
                            <Text style={styles.blueBoxDesc}>   - Ayam Goreng</Text>
                            <Text style={styles.blueBoxDesc}>   - Sup Ayam</Text>
                            <Text/>
                            <Text style={styles.blueBoxDesc}> Take : 21 januari 2018</Text>
                            <Text style={styles.blueBoxDesc}> Note :tidak pedas</Text>
                        </View>
                        <Image
                            style={styles.blueboxPic}
                            source={require('../../img/icon/akunBabi.png')} />
                    </View>
                </View>

                <View style={styles.blueBox}>
                    <View style={styles.blueBoxPicText}>

                        <View style={styles.blueBoxText}>
                            <View style={styles.boxTitle}>
                                <Text style={styles.blueBoxTitle}>Adit Sanajaya</Text>
                            </View>
                            <Text style={styles.blueBoxDesc}>   - Ayam Goreng</Text>
                            <Text style={styles.blueBoxDesc}>   - Sup Ayam</Text>
                            <Text/>
                            <Text style={styles.blueBoxDesc}> Take : 21 januari 2018</Text>
                            <Text style={styles.blueBoxDesc}> Note :tidak pedas</Text>
                        </View>
                        <Image
                            style={styles.blueboxPic}
                            source={require('../../img/icon/akunBabi.png')} />
                    </View>
                </View>
            </ScrollView>




            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#C4C4C4',
    },


    whiteBox:{
        backgroundColor:'white',
        paddingTop: 50,
        flexDirection: 'row',
        paddingLeft:30,
        paddingBottom:10,
        alignItems: 'center'
    },
    whiteBoxtext:{
        paddingLeft:15
    },


    // untuk BLUEBOX CSS
    blueBox: {
        marginTop:30,
        backgroundColor:'#008FAE',
        marginHorizontal:30,
        paddingVertical:10,
        paddingHorizontal:10,
        width:300,
        borderRadius:10
    },
    blueboxPic:{
        marginRight:100
    },

    blueBoxPicText: {
        flexDirection:'row',
        backgroundColor:'#008FAE',
        alignItems:'center'
    },
    blueBoxText: {
        width:210,
        paddingLeft: 10
    },
    // tolong dipisah anatara title nanti dengan view agar ada garis bawah
    boxTitle:{
        borderBottomWidth:1,
        borderColor:'white',
        width:200
    },
    blueBoxTitle: {
        color:'white',
        fontSize: 18,
        borderColor:'black',

    },
    blueBoxDesc:{

        paddingLeft:5,
        fontSize: 12,
        color: 'white'
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

