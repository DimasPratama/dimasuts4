/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Button, Platform, StyleSheet, Text, View, TouchableOpacity,Image,TextInput} from 'react-native';
import {Navigation} from "react-native-navigation";

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev forMenu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press forMenu button for dev forMenu',
});


export default class App extends Component {


  onPress = () => {
      console.log('push to Menu')
      Navigation.push(this.props.componentId, {
          component: {
              name: 'Menu',

          }
      });


  }



  render() {
    return (
        <View style={styles.MainContainer}>

            {/*header*/}
            <View style={styles.HeaderList}>
                <Text style={{fontSize:22, color:'#FF9345',fontWeight: '700'}}>
                    Add New Menu
                </Text>
            </View>

            {/*untuk isi membuat menu*/}
            <View style={{backgroundColor: '#C4C4C4',alignItems:'center',justifyContent: 'center',flex:1,borderTopLeftRadius:10,borderTopRightRadius:10}}>

                <View style={{backgroundColor: 'white',width: '80%',height: '80%',alignItems:'center',borderRadius:10}}>
                <Image
                    source={require('./img/icon/FotoUpload.png')}
                    style={{marginTop:30}}
                />
                    <View style={styles.inputContainer}>

                        <View>
                            <TextInput
                              style={{fontSize:16,color:'black',width:200}}
                              returnKeyType="next"
                            />

                        </View>

                            <Text style={{color:'#008FAE',fontSize:12,marginRight:40}}>Menu Name</Text>

                    </View>

                    <View style={styles.inputContainer}>

                        <View>
                            <TextInput
                                style={{fontSize:16,color:'black',width:200}}
                                placeholder="<Number>"
                                returnKeyType="next"
                            />

                        </View>

                        <Text style={{color:'#008FAE',fontSize:12,marginRight:40}}>First Stock</Text>

                    </View>

                        <View >
                            <Text style={styles.textDesc}> Description :</Text>
                            <TextInput
                                style={styles.textInput}
                                placeholderTextColor='black'
                                placeholder="This is description"
                                multiline={true}/>
                        </View>



                    <View style={{alignItems:'center'}}>
                        <TouchableOpacity style={styles.buttonAll} onPress={this.onPress}>
                            <Text style={{color:'white',fontSize:18}}>Add Menu</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
    HeaderList:{
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom:20,
        paddingTop: 30,
        backgroundColor: 'white',
        width:400



    },

    MainContainer: {
        flex: 1,
        paddingTop: (Platform.OS === 'ios') ? 20 : 0,
    },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
    inputContainer:{
        flexDirection:'row',
        width:270,
        marginBottom: 20,
        borderBottomWidth:1,
        borderBottomColor: 'black',
        paddingVertical:5,
        marginTop: 20
    },
    buttonAll: {
        marginVertical:10,
        backgroundColor:'#008FAE',
        alignItems:'center',
        justifyContent:'center',
        width:190,
        height:40,
        borderRadius: 20
    },
    textInput:{
        paddingLeft:10,
        marginBottom:10,
        width:240,
        height:105,
        backgroundColor: '#E8E8E8',
        color: 'black',
        borderRadius:5

    },
    textDesc:{

        paddingBottom:10,
        color:'#008FAE',
        fontSize:12,

    },
});
